#! /usr/bin/env python
# -*- coding:utf-8 -*-

import os, datetime, re, yaml, shutil, glob
import json
import argparse
from shutil import make_archive


def recuperation_normes():
    liste_normes = {3, 4, 5, 6, 7, 8, 9, 10, 11, 12}
    return liste_normes


def creation_test(norme, envoi, test, donnees):
    #print("test" + test.__str__())
    #for key, value in donnees.items():
    #    print(key, value)

    nom_enveloppe = ''
    nom_metier = ''

    if 'enveloppe' in donnees:
        nom_enveloppe = creation_fichier_enveloppe(envoi, donnees)
        shutil.copyfile('in/enveloppe.xml', 'temp/%s' % nom_enveloppe)
        shutil.copyfile('in/enveloppe.xml', '/tmp/enveloppe.xml')
        contenu_fichier_enveloppe("temp/" + nom_enveloppe, envoi, donnees)

    if 'metier' in donnees:

        if 'incoherence' in donnees and donnees['incoherence'] == 'enveloppemetier':
            nom_metier = creation_fichier_metier_incoherent(envoi, donnees)
        else:
            nom_metier = creation_fichier_metier(envoi, donnees)

        shutil.copyfile('in/metier.xml', 'temp/%s' % nom_metier)
        shutil.copyfile('in/metier.xml', '/tmp/metier.xml')
        contenu_fichier_metier("temp/" + nom_metier, envoi, donnees)

    if ('modif_balise_enveloppe' in donnees and 'enveloppe' in donnees) or \
            ('ajout_balise_metier' in donnees and 'metier' in donnees) or \
            ('modif_balise_metier' in donnees and 'enveloppe' in donnees) or \
            ('ajout_balise_enveloppe' in donnees and 'metier' in donnees):
        modification_balise(nom_enveloppe, nom_metier, donnees)


    if 'incoherence' in donnees and donnees['incoherence'] == 'metieracte':
        shutil.copyfile('in/acte_ou_annexe.%s' % donnees['format'], 'temp/%s' % creation_document_metier_different(envoi, donnees))
    else:
        shutil.copyfile('in/acte_ou_annexe.%s' % donnees['format'], 'temp/%s' % creation_document_metier(envoi, donnees))

    if 'compress' in donnees:
        contenu_compressezip(norme)
    else:
        contenu_compresse(norme)


def chargement(chemin):
    dicoval = {}
    try:
        fichier = open(chemin, 'r')
        lignes = fichier.readlines()

        for lig in lignes:
            if not lig.startswith('#') and lig != '\n':
                resultat = lig.rstrip('\r\n').split('=')
                dicoval[resultat[0]] = resultat[1]
        fichier.close()
    except:
        sys.exit('Le fichier %s est introuvable' % chemin)

    return dicoval


def creation_fichier_enveloppe(envoi, donnees):

    if 'enveloppe' in donnees:
        if 'mal_nomme' in donnees and donnees['mal_nomme'] == 'enveloppe':
            nom = 'EACT--0e@!--%s-%s.%s' % (conf['date'].strftime('%Y%m%d'), str(envoi), donnees['enveloppe'])

        elif 'mal_nomme' in donnees and donnees['mal_nomme'] == 'enveloppe_tiret':
            nom = 'EACT---%s-%s-%s.%s' % (conf['siren'],conf['date'].strftime('%Y%m%d'), str(envoi), donnees['enveloppe'])

        elif 'mal_nomme' in donnees and donnees['mal_nomme'] == 'enveloppe_quadrigramme':
            nom = 'BALO--%s--%s-%s.%s' % (
                conf['siren'], conf['date'].strftime('%Y%m%d'), str(envoi), donnees['enveloppe'])

        elif 'mal_nomme' in donnees and donnees['mal_nomme'] == 'enveloppe_siren':
            nom = 'EACT--%s--%s-%s.%s' % (
                '963258741', conf['date'].strftime('%Y%m%d'), str(envoi), donnees['enveloppe'])

        elif 'mal_nomme' in donnees and donnees['mal_nomme'] == 'enveloppe_siren10':
            nom = 'EACT--%s--%s-%s.%s' % (
                '9632587410', conf['date'].strftime('%Y%m%d'), str(envoi), donnees['enveloppe'])

        elif 'mal_nomme' in donnees and donnees['mal_nomme'] == 'enveloppe_siren8':
            nom = 'EACT--%s--%s-%s.%s' % (
                '96325874', conf['date'].strftime('%Y%m%d'), str(envoi), donnees['enveloppe'])

        elif 'mal_nomme' in donnees and donnees['mal_nomme'] == 'enveloppe_date':
            nom = 'EACT--%s--%s-%s.%s' % (
                conf['siren'], conf['date'].strftime('%Y%m%d')[:-4], str(envoi), donnees['enveloppe'])

        elif 'mal_nomme' in donnees and donnees['mal_nomme'] == 'enveloppe_numero':
            nom = 'EACT--%s--%s-%s.%s' % (
                conf['siren'], conf['date'].strftime('%Y%m%d'), 'xx', donnees['enveloppe'])

        elif 'incoherence' in donnees and donnees['incoherence'] == 'enveloppexml':
            nom = 'EACT--%s--%s-%s.%s' % (
                '9632587411', conf['date'].strftime('%Y%m%d'), str(envoi), donnees['enveloppe'])

        elif 'date_enveloppe' in donnees:
            nom = 'EACT--%s--%s-%s.%s' % (
                conf['siren'], donnees['date_enveloppe'], str(envoi), donnees['enveloppe'])

        else:
            nom = 'EACT--%s--%s-%s.%s' % (
                conf['siren'], conf['date'].strftime('%Y%m%d'), str(envoi), donnees['enveloppe'])
    else:
        nom = 'EACT--%s--%s-%s.xxx' % (conf['siren'], conf['date'].strftime('%Y%m%d'), str(envoi))

    return nom

def creation_fichier_enveloppe_correcte(envoi, donnees):
    nom = 'EACT--%s--%s-%s.%s' % (
        conf['siren'], conf['date'].strftime('%Y%m%d'), str(envoi), donnees['enveloppe'])
    return nom

def creation_fichier_metier(envoi, donnees):


    if 'metier' in donnees:
        if 'mal_nomme' in donnees and donnees['mal_nomme'] == 'metier':
            nom = '0e@?-00*000-' + conf['date'].strftime('%Y%m%d') + '-' + conf['libelle'].replace('?',str(envoi)) + '-' + donnees['nature'][2:] + '-1-1_0.' + donnees['metier']

        elif 'mal_nomme' in donnees and donnees['mal_nomme'] == 'metier_tiret':
            nom = 'EACT---%s-%s-%s.%s' % (conf['siren'],conf['date'].strftime('%Y%m%d'), str(envoi), donnees['metier'])

        elif 'mal_nomme' in donnees and donnees['mal_nomme'] == 'metier_quadrigramme':
            nom = 'BALO--%s--%s-%s.%s' % (
                conf['siren'], conf['date'].strftime('%Y%m%d'), str(envoi), donnees['metier'])

        elif 'mal_nomme' in donnees and donnees['mal_nomme'] == 'metier_siren':
            nom = 'EACT--%s--%s-%s.%s' % (
                '963258741', conf['date'].strftime('%Y%m%d'), str(envoi), donnees['metier'])

        elif 'mal_nomme' in donnees and donnees['mal_nomme'] == 'metier_siren10':
            nom = 'EACT--%s--%s-%s.%s' % (
                '9632587410', conf['date'].strftime('%Y%m%d'), str(envoi), donnees['metier'])

        elif 'mal_nomme' in donnees and donnees['mal_nomme'] == 'metier_siren8':
            nom = 'EACT--%s--%s-%s.%s' % (
                '96325874', conf['date'].strftime('%Y%m%d'), str(envoi), donnees['metier'])

        elif 'mal_nomme' in donnees and donnees['mal_nomme'] == 'metier_date':
            nom = 'EACT--%s--%s-%s.%s' % (
                conf['siren'], conf['date'].strftime('%Y%m%d')[:-4], str(envoi), donnees['metier'])

        elif 'mal_nomme' in donnees and donnees['mal_nomme'] == 'metier_numero':
            nom = 'EACT--%s--%s-%s.%s' % (
                conf['siren'], conf['date'].strftime('%Y%m%d'), 'xx', donnees['metier'])

        elif 'incoherence' in donnees and donnees['incoherence'] == 'metierxml':
            nom = 'EACT--%s--%s-%s.%s' % (
                '9632587411', conf['date'].strftime('%Y%m%d'), str(envoi), donnees['metier'])

        elif 'date_metier' in donnees:
            nom = 'EACT--%s--%s-%s.%s' % (
                conf['siren'], donnees['date_metier'], str(envoi), donnees['metier'])

        else:
            nom = conf['departement'] + '-' + conf['siren'] + '-' + conf['date'].strftime('%Y%m%d') + '-' + conf[
                'libelle'].replace('?', str(envoi)) + '-' + donnees['nature'][2:] + '-1-1_0.' + donnees['metier']
    else:
        nom = conf['departement'] + '-' + conf['siren'] + '-' + conf['date'].strftime('%Y%m%d') + '-' + conf[
            'libelle'].replace('?', str(envoi)) + '-' + donnees['nature'][2:] + '-1-1_0.xml'

    return nom


def creation_fichier_metier_incoherent(envoi, donnees):
    nom = conf['departement'] + '-963258741-' + conf['date'].strftime('%Y%m%d') + '-' + conf[
            'libelle'].replace('?', str(envoi)) + '-' + donnees['nature'][2:] + '-1-1_0.xml'

    return nom

def creation_document_metier(envoi, donnees):


    if 'typage' in donnees and donnees['typage'] == '3char':
        nom = '2_A-' + conf['departement'] + '-' + conf['siren'] + '-' + conf['date'].strftime('%Y%m%d') + '-' + conf[
            'libelle'].replace('?', str(envoi)) + '-' + donnees['nature'][2:] + '-1-1_1.' + donnees['format']

    elif 'acte' in donnees and donnees['acte'] == 'tiret':
        nom = '22_AC--' + conf['departement'] + conf['siren'] + '-' + conf['date'].strftime('%Y%m%d') + '-' + conf[
            'libelle'].replace('?', str(envoi)) + '-' + donnees['nature'][2:] + '-1-1_1.' + donnees['format']

    else:
        nom = '22_AC-' + conf['departement'] + '-' + conf['siren'] + '-' + conf['date'].strftime('%Y%m%d') + '-' + conf[
        'libelle'].replace('?', str(envoi)) + '-' + donnees['nature'][2:] + '-1-1_1.' + donnees['format']

    return nom

def creation_document_metier_different(envoi, donnees):
    nom = '22_AC-' + conf['departement'] + '-963258741-' + conf['date'].strftime('%Y%m%d') + '-' + conf[
        'libelle'].replace('?', str(envoi)) + '-' + donnees['nature'][2:] + '-1-1_1.' + donnees['format']

    return nom

def contenu_fichier_enveloppe(chemin, envoi, donnees):
    fichier = open(chemin, 'r')
    contenu_entier = fichier.readlines()
    fichier.close()

    contenu_sansretour = [s.rstrip('\n') for s in contenu_entier]

    contenu = ''.join(contenu_sansretour)
    #print(contenu)
    liste_balises = {1, 2, 3, 4, 5, 6, 7}

    if 'replace_siren_enveloppe' in donnees:
        contenu = contenu.replace('<4>', '963258741')

    if 'vide_balise_enveloppe' in donnees and donnees['vide_balise_enveloppe'] == 'toutes':
        for balise in liste_balises:
            contenu = contenu.replace('<' + balise.__str__() + '>', '')

    elif 'vide_balise_enveloppe' in donnees and donnees['vide_balise_enveloppe'] != 'toutes':
        liste_balises = donnees['vide_balise_enveloppe'].split(';')
        for balise in liste_balises:
            contenu = contenu.replace('<' + balise.__str__() + '>', '')

        contenu = contenu.replace('<1>', conf['nature'])
        contenu = contenu.replace('<2>', conf['arrondissement'])
        contenu = contenu.replace('<3>', conf['departement'])
        contenu = contenu.replace('<4>', conf['siren'])
        contenu = contenu.replace('<5>', conf['nom'])
        contenu = contenu.replace('<6>', conf['email'])

    else:
        contenu = contenu.replace('<1>', conf['nature'])
        contenu = contenu.replace('<2>', conf['arrondissement'])
        contenu = contenu.replace('<3>', conf['departement'])
        contenu = contenu.replace('<4>', conf['siren'])
        contenu = contenu.replace('<5>', conf['nom'])
        contenu = contenu.replace('<6>', conf['email'])

    if 'mauvaise_ref' in donnees and donnees['mauvaise_ref'] == 'metier':
        contenu = contenu.replace('<7>',
                                        re.sub(r'\d\d\d\d\d\d', '010101', creation_fichier_metier(envoi, donnees)))
    else:
        contenu = contenu.replace('<7>', creation_fichier_metier(envoi, donnees))

    fichier = open(chemin, 'w')
    fichier.write(contenu)
    fichier.close()


def contenu_fichier_metier(chemin, envoi, donnees):
    fichier = open(chemin, 'r')
    contenu_entier = fichier.readlines()
    fichier.close()

    contenu_sansretour = [s.rstrip('\n') for s in contenu_entier]

    contenu = ''.join(contenu_sansretour)
    #print(contenu)
    liste_balises = {1, 2, 3, 4, 5, 6, 7, 8}

    if 'vide_balise_metier' in donnees and donnees['vide_balise_metier'] == 'toutes':
        for balise in liste_balises:
            contenu = contenu.replace('<' + balise.__str__() + '>', '')

    elif 'vide_balise_metier' in donnees and donnees['vide_balise_metier'] != 'toutes':
        liste_balises = donnees['vide_balise_metier'].split(';')
        for balise in liste_balises:
            contenu = contenu.replace('<' + balise.__str__() + '>', '')

        contenu = contenu.replace('<1>', donnees['nature'][0:1])
        contenu = contenu.replace('<2>', conf['libelle'].replace('?', str(envoi)))
        contenu = contenu.replace('<3>', str(conf['date']))
        contenu = contenu.replace('<4>', str(donnees['matiere'])[0:1])
        contenu = contenu.replace('<5>', str(donnees['matiere'])[2:3])
        contenu = contenu.replace('<6>', conf['libelle'].replace('?', str(envoi)))

    else:
        contenu = contenu.replace('<1>', donnees['nature'][0:1])
        contenu = contenu.replace('<2>', conf['libelle'].replace('?', str(envoi)))
        contenu = contenu.replace('<3>', str(conf['date']))
        contenu = contenu.replace('<4>', str(donnees['matiere'])[0:1])
        contenu = contenu.replace('<5>', str(donnees['matiere'])[2:3])
        contenu = contenu.replace('<6>', conf['libelle'].replace('?', str(envoi)))

    if 'mauvaise_ref' in donnees and donnees['mauvaise_ref'] == 'document':
        contenu = contenu.replace('<7>',
                                        re.sub(r'\d\d\d\d\d\d', '010101', creation_document_metier(envoi, donnees)))
    else:
        contenu = contenu.replace('<7>', creation_document_metier(envoi, donnees))

    ajout = ""
    if 'nombre_annexe' in donnees:
        contenu = contenu.replace('<8>', str(donnees['nombre_annexe']))
        contenu = contenu + ajout_annexe(donnees)
    else:
        contenu = contenu.replace('<8>">', '0"/></actes:Acte>')

    fichier = open(chemin, 'w')
    fichier.write(contenu)
    fichier.close()


def modification_balise(nom_enveloppe, nom_metier, donnees):
    if 'modif_balise_enveloppe' in donnees:
        nom_fichier = "temp/" + nom_enveloppe

    elif 'modif_balise_metier' in donnees:
        nom_fichier = "temp/" + nom_metier

    elif 'ajout_balise_enveloppe' in donnees:
        nom_fichier = "temp/" + nom_enveloppe

    else:
        nom_fichier = "temp/" + nom_metier

    fichier = open(nom_fichier, 'r')
    contenu_entier = fichier.readlines()
    fichier.close()

    contenu_sansretour = [s.rstrip('\n') for s in contenu_entier]

    contenu = ''.join(contenu_sansretour)
    #print(contenu)

    if 'modif_balise_enveloppe' in donnees:
        liste_balises = donnees['modif_balise_enveloppe'].split(';')
        contenu = contenu.replace(liste_balises[0], liste_balises[1])

    elif 'modif_balise_metier' in donnees:
        liste_balises = donnees['modif_balise_metier'].split(';')
        contenu = contenu.replace(liste_balises[0], liste_balises[1])

    elif 'ajout_balise_enveloppe' in donnees:
        contenu = contenu + donnees['ajout_balise_enveloppe']

    else:
        contenu = contenu + donnees['ajout_balise_metier']

    fichier = open(nom_fichier, 'w')
    fichier.write(contenu)
    fichier.close()


def ajout_annexe(donnees):
    ajout = ""
    liste_annexe = donnees['annexe'].split(';')

    index = 2
    for annexe in liste_annexe:
        nom_annexe = '22_AC-' + conf['departement'] + '-' + conf['siren'] + '-' + conf['date'].strftime('%Y%m%d') + '-' + conf[
            'libelle'].replace('?', str(envoi)) + '-' + donnees['nature'][2:] + '-1-1_' + str(index) + '.' + annexe[len(
            annexe) - 4:]
        nom_annexe = nom_annexe.replace('..', '.')  # à cause du -4 pour le docx (norme 2 - test 6)

        shutil.copyfile('in/%s' % annexe, 'temp/%s' % nom_annexe)

        ajout += "<actes:Annexe><actes:NomFichier>%s</actes:NomFichier></actes:Annexe>" % nom_annexe
        index = index + 1

    ajout += "</actes:Annexes></actes:Acte>"
    return ajout


def contenu_compresse(norme):
    nom_enveloppe = creation_fichier_enveloppe(envoi, donnees)

    if 'incoherence' in donnees and donnees['incoherence'] == 'enveloppexml':
        nom_enveloppe = creation_fichier_enveloppe_correcte(envoi, donnees)


    if 'itc' in donnees:
        os.system('cd temp && tar -czf %s-%s.tar.gz *' % ('AQW', nom_enveloppe[:-4]))
        rapport['AWQ' + "-" + nom_enveloppe[:-4] + ".tar.gz"] = donnees['resultat']

    elif 'incoherence' in donnees and donnees['incoherence'] == 'enveloppetgz':
        nom_enveloppe = 'EACT--%s--%s-%s.xxx' % ('963258741', conf['date'].strftime('%Y%m%d'), str(envoi))
        os.system('cd temp && tar -czf %s-%s.tar.gz *' % (conf['itc'], nom_enveloppe[:-4]))
        rapport[conf['itc'] + "-" + nom_enveloppe[:-4] + ".tar.gz"] = donnees['resultat']

    else:
        os.system('cd temp && tar -czf %s-%s.tar.gz *' % (conf['itc'], nom_enveloppe[:-4]))
        rapport[conf['itc'] + "-" + nom_enveloppe[:-4] + ".tar.gz"] = donnees['resultat']

    os.system('cp temp/*.tar.gz out/norme%s' % norme)
    for fichier in glob.glob('temp/*'):
        os.remove(fichier)

def contenu_compressezip(norme):
    nom_enveloppe = creation_fichier_enveloppe(envoi, donnees)
    os.system('cd temp && zip %s-%s.zip *' % (conf['itc'], nom_enveloppe[:-4]))
    os.system('cp temp/*.zip out/norme%s' % norme)
    rapport[conf['itc'] + "-" + nom_enveloppe[:-4] + ".zip"] = donnees['resultat']
    for fichier in glob.glob('temp/*'):
        os.remove(fichier)

def chargement_tests_normes(norme):
    fichier_norme = open("normes/norme%s.yml" % norme)
    tests = yaml.safe_load(fichier_norme)
    #print(tests.items())
    fichier_norme.close()

    repertoire_norme = 'out/norme%s' % (norme)

    if not os.path.exists(repertoire_norme):
        os.makedirs(repertoire_norme)

    return tests

def rapportnormes():
    fichier = open("out/rapportnormes", 'w')
    json.dump(rapport, fichier)
    fichier.close()

def chargementconfjson(chemin):
    fichier = open(chemin,'r')
    config = json.load(fichier)
    #print(config)
    return config

def remove_newlines(fname):
    flist = open(fname).readlines()
    return [s.rstrip('\n') for s in flist]



if __name__ == "__main__":

    if not os.path.exists('temp'):
        os.makedirs('temp')

    if not os.path.exists('out'):
        os.makedirs('out')

    configpath = "/config/config.json"
    if not os.path.exists(configpath):
        configpath="config.json"

    if not os.path.exists(configpath):
        print("Attention aucun fichier de config trouve !!")
        quit(0)


    conf = chargementconfjson(configpath)


    if conf['date'] == '00000000':
        conf['date'] = datetime.date.today()
    #conf = chargement('config')

    liste_normes = recuperation_normes()  # récupération des normes
    #norme = 32
    envoi = 1

    if 'id' in conf:
        envoi = int(conf['id'])

    #print("envoi : " + envoi.__str__())
    #exit(0)

    rapport={}

    for norme in liste_normes:
        print("Norme : " + norme.__str__())
        tests = chargement_tests_normes(norme)
        for test, donnees in tests.items():  # création des tests
            print("Test : " + test.__str__())
            if tests == {}:
                break
            creation_test(norme, envoi, test, donnees)  # création du test

            envoi = envoi + 1

        #norme = norme + 1

    shutil.rmtree('temp')
    rapportnormes()
