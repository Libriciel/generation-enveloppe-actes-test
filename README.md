# Génération des enveloppes métiers actes

Ce programme permet de générer des enveloppes métiers actes en vue de tester le bon respect des normes du CdC actes 2.2.


Il retourne des enveloppes et un rapport indiquant pour chacune d’entre elles si elles doivent être acceptées ou rejetées.

## Appel du docker

Récupération du docker
```bash
docker  pull generation-enveloppe-actes-test
```

En créeant l'image en local :
```bash
$ cd /tmp
$ git clone https://gitlab.adullact.net/Libriciel/generation-enveloppe-actes-test.git
$ cd generation-enveloppe-actes-test
$ docker build . -t generation-enveloppe-actes-test:1.3
```

```bash
docker run --rm -it -v /tmp/conf.json:/config/config.json -v /tmp/out/:/app/out/ generation-enveloppe-actes-test
```

en mode `daemon` :
```bash
docker run --rm -it -v /tmp/conf.json:/config/config.json -v /tmp/out/:/app/out/ -e DAEMON=true generation-enveloppe-actes-test

```

Exemple de fichier de configuration :
```json
{
	"libelle": "HOMOLAUDIT?",
	"date": "00000000",
	"siren": "214400330",
	"nom": "LSC",
	"nature": "31",
	"arrondissement": "2",
	"departement": "034",
	"email": "p.viver@libriciel.coop",
	"itc": "LSC"
	"id": 50
}
```

**Le trigramme,`itc`, ne doit jamais être AQW**
**Le SIREN, `siren`, ne doit jamais être 963258741**

Le paramètre `id` est optionnel. Il peut être entre `"` ou non.


Il est nécessaire d'avoir un volume pour récupérer les enveloppes générées. Il doit obligatoirement être "mappé" dans le dossier `/app/out`.


## Rapport de sortie

Exemple de rapport généré :

```json
{
    "LSC-EACT--214400228--20180709-1.tar.gz":"rejetee",
    "LSC-EACT--214400228--20180709-2.tar.gz":"rejetee",
    "LSC-EACT--214400228--20180709-3.tar.gz":"rejetee",
    "LSC-EACT--0e@!--20180709-4.tar.gz":"rejetee",
    "LSC-EACT--214400228--20180709-5.tar.gz":"rejetee",
    "LSC-EACT--214400228--20180709-6.tar.gz":"acceptee",
    "LSC-EACT--214400228--20180709-7.tar.gz":"rejetee",
    "LSC-EACT--214400228--20180709-8.tar.gz":"rejetee",
    "LSC-EACT--214400228--20180709-9.tar.gz":"rejetee",
    "LSC-EACT--214400228--20180709-10.tar.gz":"rejetee",
    "LSC-EACT--214400228--20180709-11.tar.gz":"rejetee",
    "LSC-EACT--214400228--20180709-12.tar.gz":"rejetee",
    "LSC-EACT--214400228--20180709-13.tar.gz":"rejetee",
    "LSC-EACT--214400228--20180709-14.tar.gz":"rejetee",
    "LSC-EACT--214400228--20180709-15.tar.gz":"rejetee",
    "LSC-EACT--214400228--20180709-16.tar.gz":"rejetee",
    "LSC-EACT--214400228--20180709-17.tar.gz":"acceptee",
    "LSC-EACT--214400228--20180709-18.tar.gz":"acceptee",
    "LSC-EACT--214400228--20180709-19.tar.gz":"acceptee",
    "LSC-EACT--214400228--20180709-20.tar.gz":"rejetee",
    "LSC-EACT--214400228--20180709-21.tar.gz":"rejetee",
    "LSC-EACT--214400228--20180709-22.tar.gz":"rejetee",
    "LSC-EACT--214400228--20180709-23.tar.gz":"rejetee"
}
```



## Coopération

Ce projet est réalisé conjointement avec la société OPPIDA.

Le but étant par la suite d'enrichir la génération des enveloppes avec de nouveaux tests et de nouvelles normes à controler.


## Les possibilités

| Variable | Contenue | Action |
| -------- | -------- | ------ |
| modif_balise_enveloppe | EnveloppeCLMISILL;EnveloppeOPPIDA | Remplace la chaîne de caractères `EnveloppeCLMISILL` par `EnveloppeOPPIDA` dans le XML enveloppe. |
| modif_balise_metier | CodeNatureActe;AucuneNatureActe | Remplace la chaîne de caractères `CodeNatureActe` par `AucuneNatureActe` dans le XML metier. |
| ajout_balise_enveloppe | \<actes:BaliseNonvalide>texte non valide</actes:BaliseNonvalide> | Ajoute une balise dans le XML enveloppe |
| ajout_balise_metier | \<actes:BaliseNonvalide>texte non valide</actes:BaliseNonvalide> | Ajoute une balise dans le XML métier |
| vide_balise_metier | `toutes` | Cela ne remplie aucune des balises du XML métier |
| vide_balise_metier | 1;2;5 | Cela ne remplie pas les balises listées et séparée par ; dans le XML métier |
| vide_balise_enveloppe | `toutes` | Cela ne remplie aucune des balises du XML enveloppe |
| vide_balise_enveloppe | 1;2;5 | Cela ne remplie pas les balises listées et séparée par ; dans le XML enveloppe |
| nombre_annexe avec annexe | réciproquement 2 et acte_ou_annexe.png;acte_ou_annexe.txt | Ajoute les annexes dans l'enveloppe |
| metier | xml | Si cette variable est absente, le fichier XML métier ne sera pas ajouté dans l'enveloppe |
| enveloppe | xml | Si cette variable est absente, le fichier XML enveloppe ne sera pas ajouté dans l'enveloppe |
| mal_nomme | enveloppe/metier | Stipule quel fichier XML doit être mal nommé |
| mauvaise_ref | document | La référence vers l'acte principale sera erronée |
| mauvaise_ref | metier | La référence vers le document métier sera erronée |
| mauvaise_ref | enveloppe | La référence vers le fichier XML enveloppe sera erronée |
| compress | zip | Cela zip l'archive |
| itc | erronne | Cela va mettre le trigramme à AQW |
| mal_nomme | enveloppe_tiret | Dans le nom du fichier enveloppe, tar.gz, les tirets ne correspondent pas à la norme |
| mal_nomme | enveloppe_quadrigramme | Dans le nom du fichier enveloppe, tar.gz, les quadrigramme EACT est remplacé par BALO |
| mal_nomme | enveloppe_siren | Dans le nom du fichier enveloppe, tar.gz, le siren est remplacé par 963258741 |
| mal_nomme | enveloppe_siren10 | Dans le nom du fichier enveloppe, tar.gz, le siren est remplacé par 9632587410 |
| mal_nomme | enveloppe_siren8 | Dans le nom du fichier enveloppe, tar.gz, le siren est remplacé par 96325874 |
| mal_nomme | enveloppe_date | Dans le nom du fichier enveloppe, tar.gz, la date est amputée de 4 caractères |
| incoherence | enveloppetgz | Les noms des fichiers seront correctes. Cependant, les informations dans le nom de l'enveloppe tar.gz ne correspondront pas à celles attendues. |
| incoherence | enveloppexml | Les noms des fichiers seront correctes. Cependant, les informations dans le nom de l'enveloppe xml ne correspondront pas à celles attendues. |
| incoherence | enveloppemetier | Le nom du fichier XML métier est bon dans le fichier XML enveloppe. Mais le nom du fichier XML métier ne correspondra pas à celui attendu. |
| incoherence | metieracte | Le nom du fichier acte est erroné mais sera bon le XML métier. |
| replace_siren_enveloppe | true | Le SIREN dans le fichier XML métier ne sera pas celui indiqué dans le fichier de configuration |
| typage | 3char | Le typage de l'acte sera 9_U |
