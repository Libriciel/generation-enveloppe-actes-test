# Change log

##[ 1.6.0] - en cours

### Corrections

-  Pièce principale en pdf


##[ 1.5.0] - 11/09/2018

### Evolutions

-  Mise à jour date classification


##[ 1.4.0] - 27/08/2018

### Evolutions

-  Déploiement sur Docker hub

 
## [ 1.3.0] - 27/08/2018

### Evolutions

- Il est possible de définir le numéro de départ de génération des enveloppes ;
- Nouvelles normes 10, 11 et 12 ;


## [ 1.2.1] - 24/08/2018

### Corrections

- Erreur sur la variable DAEMON


## [ 1.2.0] - 24/08/2018

### Evolutions

- Nouvelles normes 06, 07, 08 et 09 ;
- La date est initialisée avant de commencer la génération des enveloppes ;
- Le docker peut être lancé en mode daemon avec la variable d'environnement DAEMON = true


### Corrections

- Norme 05, l'enveloppe avec le mauvais trigramme était marquée "acceptee" ;



## [1.1.0] - 23/08/2018

### Evolutions

- Nouvelles normes 03, 04 et 05 ;
- Les normes testées peuvent ne pas suivre 3, 4, 5, 8, 12 ;
- Les fichiers XML ne sont plus sur une seule ligne ;
- Norme 04 : on peut vider tout ou partie des balises ;
 


### Corrections

- Le rapport ne renvoyait pas les noms des enveloppes ;


## [1.0.0] - 06/07/2018

### Evolutions

- Passage en Python 3 ;
- Dockerisation du programme d'Oppida ;
- Prise en compte du fichier de configuration en `json` ;
- Création du rapport d'enveloppe ;