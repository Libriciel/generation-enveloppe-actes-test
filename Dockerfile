FROM ubuntu:18.04

RUN apt-get update -y \
    && apt-get install -y software-properties-common \
    && apt-get update -y \
    && apt-get install -y \
    zip \
    unzip \
    curl \
    locales \
    tzdata \
    python3-pil \
    python3.8-dev \
    python-yaml \
    && rm -r /var/lib/apt/lists/*

RUN sed -i -e 's/# fr_FR.UTF-8 UTF-8/fr_FR.UTF-8 UTF-8/' /etc/locale.gen && \
    echo 'LANG="fr_FR.UTF-8"'>/etc/default/locale&& \
    set -eux && dpkg-reconfigure --frontend=noninteractive locales && \
    update-locale LANG=fr_FR.UTF-8 && \
    echo "Europe/Paris" > /etc/timezone &&\
    set -eux && dpkg-reconfigure -f noninteractive tzdata

COPY bootsrap.pypa.io.py /tmp
RUN python /tmp/bootsrap.pypa.io.py

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

COPY . /app/


COPY ./entrypoint.sh /usr/local/bin/
RUN chmod a+x /usr/local/bin/entrypoint.sh

WORKDIR /app
ENTRYPOINT ["entrypoint.sh"]
