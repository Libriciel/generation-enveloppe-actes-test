#!/usr/bin/env bash

if [ "${DAEMON}" = true ]
then
    /bin/bash
else
    python /app/main.py
fi